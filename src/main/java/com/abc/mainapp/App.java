package com.abc.mainapp;


import com.abc.beans.Movie;
import com.abc.target.ShowService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {

        ClassPathXmlApplicationContext applicationContext = new
                ClassPathXmlApplicationContext("applicationContext.xml");

        Movie movie = applicationContext.getBean(Movie.class);

        ShowService movieShow = (ShowService) applicationContext.getBean("target");

        try {
            movieShow.run(movie);
        } catch (Exception exception) {

        }

        applicationContext.close();

    }
}
