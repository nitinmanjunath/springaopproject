package com.abc.target;

import com.abc.beans.Movie;

public class ShowServiceImpl implements ShowService {


    @Override
    public String run(Movie movie) throws Exception {

        System.out.println("Movie :" + movie.getName() + " is running");

        if (!movie.getType().equalsIgnoreCase("crimethriller")){
            throw  new RuntimeException("It is not of type CrimeThriller");
        }
        System.out.println("Movie is running succesfully");

        return "success";
    }
}
