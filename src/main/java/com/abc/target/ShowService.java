package com.abc.target;

import com.abc.beans.Movie;

public interface ShowService {

    public String run(Movie movie) throws Exception;


}
