package com.abc.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

public class ShowAspect {

    public void before(JoinPoint joinPoint) {
        System.out.println("Get the ticket to watch the movie!!!!");
    }

    public void after(JoinPoint joinPoint) {
        System.out.println("Show is over,You can quit the hall!!!!");
    }

    public void around(ProceedingJoinPoint proceedingJoinPoint) {
        System.out.println("Movie is starting!!!! please keep your phones in silent mode");

        try {
            proceedingJoinPoint.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        System.out.println("Movie is completed!!!!You can switch on your phones now!!!");
    }

    public void afterReturning(JoinPoint joinPoint, Object result) {
        System.out.println("Returned from the method :" + joinPoint.getSignature().getName() + " Movie status is : " + result);
    }

    public void afterThrowing(JoinPoint joinPoint, Exception exception) {
        System.out.println("Movie is not of type: Crime thriller.Exception thrown from :"+joinPoint.getSignature().getName());
    }


}
